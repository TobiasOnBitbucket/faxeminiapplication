import java.io.IOException;
import java.util.List;

import FAXE.*;


public class FAXEMiniApplication {

	public static void main(String[] args) throws IOException {
		System.out.println("FAXEMiniApplication - Start");
		
		System.out.println("TESTING PROJECT ANNOTATIONS");
		long startTime = System.nanoTime();
        String projectRoot = "C:\\\\Users\\\\Tobias\\\\IdeaProjects\\\\FAXE-Feature_Annotation_eXtraction_Engine\\\\test\\\\testProjectBitcoinWallet\\\\";
        List<EmbeddedAnnotation> eaList = FAXE.extractEAfromRootDirectory(projectRoot);
		System.out.println("Found " +eaList.size() +" embedded annotation elements. Duration=" +((System.nanoTime()-startTime)/1000000) +"ms.");
		
		System.out.println("TESTING CODE ANNOTATIONS");
		startTime = System.nanoTime();
		String testFileCode = "src\\\\testData_codeAnnotations.txt";
		List<EmbeddedAnnotation> eaListCode = FAXE.extractEAfromSourceCode(testFileCode);
		System.out.println("Found " +eaListCode.size() +" embedded annotation elements. Duration=" +((System.nanoTime()-startTime)/1000000) +"ms.");
		
		System.out.println("TESTING FILE ANNOTATIONS");
		startTime = System.nanoTime();
		String testFileFile = "src\\\\testData_fileAnnotations.txt";
		List<EmbeddedAnnotation> eaListFile = FAXE.extractEAfromFeatureFile(testFileFile);
		System.out.println("Found " +eaListFile.size() +" embedded annotation elements. Duration=" +((System.nanoTime()-startTime)/1000000) +"ms.");
		
		System.out.println("TESTING FOLDER ANNOTATIONS");
		startTime = System.nanoTime();
		String testFileFolder = "src\\\\testData_folderAnnotations.txt";
		List<EmbeddedAnnotation> eaListFolder = FAXE.extractEAfromFeatureFolder(testFileFolder);
		System.out.println("Found " +eaListFolder.size() +" embedded annotation elements. Duration=" +((System.nanoTime()-startTime)/1000000) +"ms.");
		
		
		System.out.println("FAXEMiniApplication - End");
	}
	
}
